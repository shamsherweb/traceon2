<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Shamsher Ahmed
 * @version 1.0.1
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     * @param String $mobile number mobile number of the user
     * @param String $country country name of the user
     * @gcm_id String firebase cloud messaging id of the user
     * @return returning integer value based on successfull creation of the user.
     */
    public function createUser($mobile_number,$name, $email, $password,$country,$gcm_id) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isMobileNumberExists($mobile_number)) {
            // Generating password hasgetUserByMobileNumberh
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(mobile_number,name, email, password_hash, api_key,gcm_id,country) values(?,?,?,?,?,?,?)");
            $stmt->bind_param("issssss",$mobile_number, $name, $email, $password_hash, $api_key,$gcm_id,$country);
            $result = $stmt->execute();
            $stmt->close();
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($mobile_number, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE mobile_number = ?");
        $stmt->bind_param("s", $mobile_number);
        $stmt->execute();
        $stmt->bind_result($password_hash);
        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    public  function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT mobile_number from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        if($num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

	public function isMobileNumberExists($mobile_number) {
        $stmt = $this->conn->prepare("SELECT name from users WHERE mobile_number = ?");
        $stmt->bind_param("s", $mobile_number);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        if($num_rows > 0){
        	return true;
        }
		return false;
    }

    public function getAllDeviceList() {

        $stmt = $this->conn->prepare("SELECT mobile_number from users");
        $result = $stmt->execute();

        if ($result) {
            $res = $stmt->get_result();
            $stmt->close();
            return $res;

        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function getContactListMobileNumbers($mobile_number){

       $stmt = $this->conn->prepare("SELECT trackee from tracking WHERE tracker = ? AND status = 1");
        $stmt->bind_param("s", $mobile_number);
        $result = $stmt->execute();

        if ($result) {
            $res = $stmt->get_result();
            $stmt->close();
            return $res;

        } else {
            $stmt->close();
            return NULL;
        }
    }
    /**
     * Fetching user by mobile number
     * @param String $mobile number mobile number of user
     */
    public function getUserByMobileNumber($mobile_number) {
        $stmt = $this->conn->prepare("SELECT name, mobile_number,email, api_key, country,profile_url FROM users WHERE mobile_number = ?");
        $stmt->bind_param("s", $mobile_number);
		//$noOfFriends = getNoOfFriends($mobile_number);
		//$noOfPeopleTracking = getNumberOfFriendsTracking($mobile_number);

        if ($stmt->execute()) {
            //$user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name,$mobile_number ,$email, $api_key, $country,$profile_url);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
			$user["mobile_number"] =$mobile_number;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["country"] = $country;
			$user["profile_url"]= $profile_url;

			//$user["noOfFriends"]=$noOfFriends;
			//$user["noOfPeopleTracking"]=$noOfPeopleTracking;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


     public function getUserByEmailAddress($email_address) {
        $stmt = $this->conn->prepare("SELECT name, mobile_number,email, api_key, country,profile_url FROM users WHERE email = ?");
        $stmt->bind_param("s", $email_address);
		//$noOfFriends = getNoOfFriends($mobile_number);
		//$noOfPeopleTracking = getNumberOfFriendsTracking($mobile_number);

        if ($stmt->execute()) {
            //$user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name,$mobile_number ,$email, $api_key, $country,$profile_url);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
			$user["mobile_number"] =$mobile_number;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["country"] = $country;
			$user["profile_url"]= $profile_url;

			//$user["noOfFriends"]=$noOfFriends;
			//$user["noOfPeopleTracking"]=$noOfPeopleTracking;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getMobileNumber($api_key) {
        $stmt = $this->conn->prepare("SELECT mobile_number FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($mobile_number);
            $stmt->fetch();
            $stmt->close();
            return $mobile_number;
        } else {
            return NULL;
        }
    }

	public function isTracking($tracker, $trackee){
		$stmt = $this->conn->prepare("SELECT id FROM tracking WHERE tracker = ? AND trackee= ? ");
		$stmt->bind_param("ss",$tracker,$trackee);
        $stmt->execute();
		$stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        if($num_rows > 0){
        	return true;
        }
		return false;
	}

	public function getGcmEmail($mobile_number){
		$user = array();
		$stmt = $this->conn->prepare("SELECT gcm_id , email from users where mobile_number = ? ");
		$stmt->bind_param("s",$mobile_number);
		$result = $stmt->execute();
		if($result){
			 $stmt->bind_result($gcm_id,$email);
			 $stmt->fetch();
			 $stmt->close();
			 $user['gcm_id'] = $gcm_id;
			 $user['email'] = $email;

			 return $user;
		}else{
			return null;
		}
	}

    public function getGcmId($mobile_number){
		    $stmt = $this->conn->prepare("SELECT gcm_id  from users where mobile_number = ? ");
            $stmt->bind_param("s",$mobile_number);
            $result = $stmt->execute();
            if($result){
			    $stmt->bind_result($gcm_id);
			    $stmt->fetch();
		}
            $stmt->close();
            return $gcm_id;
	}

    public function getUserInfo($mobile_number){
		$user = array();
		$stmt = $this->conn->prepare("SELECT name,gcm_id , email from users where mobile_number = ? ");
		$stmt->bind_param("s",$mobile_number);
		$result = $stmt->execute();
		if($result){
			 $stmt->bind_result($name,$gcm_id,$email);
			 $stmt->fetch();
			 $stmt->close();
			 $user['gcm_id'] = $gcm_id;
			 $user['email'] = $email;
             $user['name'] = $name;

			 return $user;
		}else{
			return null;
		}
	}

	public function insertTrackingDetails($tracker,$trackee){
			$stmt = $this->conn->prepare("INSERT INTO tracking(tracker,trackee) values(?,?)");
			$stmt->bind_param("ss",$tracker,$trackee);
			$result = $stmt->execute();
			$stmt->close();
		if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
		}
	}


	public function uploadProfilePic($mobile_number,$profile_url){
		$stmt = $this->conn->prepare("UPDATE users set profile_url = ? where mobile_number = ?");
		$stmt->bind_param("ss",$profile_url,$mobile_number);
		$result = $stmt->execute();
		$stmt->close();
		if ($result) {
                // User successfully inserted
                return true;
        }
		return false;
	}
    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT mobile_number from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }
    /**
     * Fetching the friend list
     * @param String $mobile_number the mobile number which you want to find the contacts.
     */
    public function getFriendList($mobile_number) {
        $stmt = $this->conn->prepare("SELECT trackee from tracking WHERE tracker = ? AND status = 1");
        $stmt->bind_param("i",$mobile_number);
		$result = $stmt->execute();
        if ($result) {
            $res = $stmt->get_result();
            $stmt->close();
            return $res;

        } else {
            return 0;
        }
    }

	public function getInviteList($mobile_number) {
        $stmt = $this->conn->prepare("SELECT tracker from tracking WHERE trackee = ? AND status = 0");
        $stmt->bind_param("i",$mobile_number);
		$result = $stmt->execute();
        if ($result) {
            $res = $stmt->get_result();
            $stmt->close();
            return $res;

        }else {
            return NULL;
        }
    }

    public function getPendingInviteList($mobile_number) {
        $stmt = $this->conn->prepare("SELECT trackee from tracking WHERE tracker = ? AND status = 0");
        $stmt->bind_param("i",$mobile_number);
		$result = $stmt->execute();
        if ($result) {
            $res = $stmt->get_result();
            $stmt->close();
            return $res;

        } else {
            return NULL;
        }
    }

    /**
     * Deleting tracking user
     * @param String $task_id id of the task to delete
     */
    public function removeContact($trackee_mobile_number,$tracker_mobile_number) {
        $stmt = $this->conn->prepare("DELETE  from tracking where trackee = ? AND tracker = ?");
        $stmt->bind_param("ii",$trackee_mobile_number,$tracker_mobile_number);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Function to the tracking history of the user
     * @param int $page_limit To implement the pagination
     * @param $mobile_number Mobile Number of the user
     */

	public function getTrackingHistory($pageLimit,$mobile_number){
		$stmt = $this->conn->prepare(" SELECT latitude,longitude FROM location_history WHERE mobile_number = ? ");
		$stmt->bind_param("i",$mobile_number);
		$result = $stmt->execute();
		if($result){
			$res = $stmt->get_result();
            $stmt->close();
			return $res;
		}else{
			return NULL;
		}
	}
	/**
	 * Function to store the user  experience
	 * @param $mobile_number Mobile Number of the user
	 * @param $name full name of the user
	 * @param $suggestion user suggestions
	 */
	public function reportApp($mobile_number,$name,$suggestion){
	  $stmt = $this->conn->prepare("INSERT INTO suggestion(mobile_number,name,suggesstion) values(?,?,?)");
	  $stmt->bind_param("iss",$mobile_number,$name,$suggestion);
	  $result = $stmt->execute();

	  $stmt->close();
	  if($result){
	  	return true;
	  }
	  return false;
	}

	public function forgotPassword($email){
		require_once 'PassHash.php';
		$new_password = mt_rand(1000,9999);
		$password_hash = PassHash::hash($new_password);
		$stmt = $this->conn->prepare("UPDATE users  set password_hash = ? where email = ?");
		$stmt->bind_param("ss",$password_hash,$email);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return $new_password;
		}
		return false;
	}

	public function changPassword($mobile_number,$password){
		require_once 'PassHash.php';
		$password_hash = PassHash::hash($password);
		$stmt = $this->conn->prepare("UPDATE users  set password_hash = ? where mobile_number = ?");
		$stmt->bind_param("ss",$password_hash,$mobile_number);
		$result = $stmt->execute();
		$stmt->close();

		if($result){
			return true;
		}
		return false;
	}

	public function getNoOfFriends($mobile_number){
		$stmt = $this->conn->prepare("SELECT trackee from tracking WHERE tracker = ? AND status = 1");
        $stmt->bind_param("s",$mobile_number);

		$result = $stmt->execute();
		$stmt->store_result();
        $num_rows = $stmt->num_rows;
		$stmt->close();
        if ($result) {
            return $num_rows;
        } else {
            return 0;
        }
	}

	public function getNumberOfFriendsTracking($mobile_number){
		$stmt = $this->conn->prepare("SELECT tracker from tracking WHERE trackee = ? AND status = 1 ");
		$stmt->bind_param("s",$mobile_number);

		$result = $stmt->execute();
		$stmt->store_result();
        $num_rows = $stmt->num_rows;
		$stmt->close();
        if ($result) {
            return $num_rows;
        } else {
            return 0;
        }
	}

	public function updateProfile($mobile_number,$name,$email){
		$stmt = $this->conn->prepare("UPDATE users  set name = ? ,email = ?  where mobile_number = ?");
		$stmt->bind_param("sss",$name,$email,$mobile_number);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return true;
		}
		return false;
	}

	public function updateGcm($mobile_number,$gcmId){
		$stmt = $this->conn->prepare("UPDATE users  set gcm_id = ?   where mobile_number = ?");
		$stmt->bind_param("ss",$gcmId,$mobile_number);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return true;
		}
		return false;
	}
	public function insertLocationHistory($mobile_number,$latitude,$longitude,$locationtime){

		 $stmt = $this->conn->prepare("INSERT INTO  location_history(mobile_number,latitude,longitude,location_time) values(?,?,?,?)");

		 $stmt->bind_param("idds",$mobile_number,$latitude,$longitude,$locationtime);

		 $result = $stmt->execute();
		 $stmt->close();
		 if($result){
			return true;
		 }else{
			return false;
		 }
	}

	public function getLocationHistory($mobile_number,$page){

		$num_of_records_per_page = 10;
		$start_from = ($page -1) * $num_of_records_per_page;

		$stmt = $this->conn->prepare("SELECT latitude,longitude,location_time FROM location_history WHERE mobile_number = ? && DATE(location_time) = DATE(NOW()) ORDER BY location_time DESC LIMIT ?,?");
		$stmt->bind_param("iii",$mobile_number,$start_from,$num_of_records_per_page);

		$result = $stmt->execute();
		if($result){
			$res = $stmt->get_result();
            $stmt->close();
			return $res;
		}else{
			return NULL;
		}
	}

	public function getTimeLine($mobile_number){
		$stmt = $this->conn->prepare("SELECT latitude,longitude,location_time FROM location_history WHERE mobile_number = ?  && DATE(location_time) = DATE(NOW()) ORDER BY location_time ASC");
		$stmt->bind_param("i",$mobile_number);

		$result = $stmt->execute();
		if($result){
			$res = $stmt->get_result();
            $stmt->close();
			return $res;
		}else{
			return NULL;
		}
	}

	public function getData($mobile_number){
		$stmt = $this->conn->prepare("SELECT latitude,longitude,location_time FROM location_history WHERE mobile_number = ? && DATE(location_time) = DATE(NOW()) ORDER BY location_time DESC ");
		$stmt->bind_param("i",$mobile_number);

		$result = $stmt->execute();
		if($result){
			$res = $stmt->get_result();
            $stmt->close();
			return $res;
		}else{
			return NULL;
		}
	}

	public function acceptRequest($tracker ,$trackee){

		$stmt = $this->conn->prepare("UPDATE tracking  set status = 1 WHERE tracker = ? AND trackee = ?");
		$stmt->bind_param("ii",$tracker,$trackee);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return true;
		}else{
			return false;
		}
	}

    public function updateContacts($contacts,$mobile_number){

		$stmt = $this->conn->prepare("UPDATE users  set contacts = ?   where mobile_number = ?");
		$stmt->bind_param("ss",$contacts,$mobile_number);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return true;
		}
		return false;
	}

    public function getContacts($mobile_number){
        $stmt = $this->conn->prepare("SELECT contacts from users WHERE mobile_number = ?");
        $stmt->bind_param("s", $mobile_number);
        $result = $stmt->execute();
        if ($result) {
           $stmt->bind_result($contacts);
           $stmt->fetch();
           $stmt->close();
            return $contacts;

        } else {
            $stmt->close();
            return NULL;
        }
    }




    public function insertTracingHistory($tracker,$trackee,$lat,$lng,$notification_id){
            $status = 0;
            $stmt = $this->conn->prepare("INSERT INTO tracing_history(tracker,trackee, lat, lng,status,notification_id) values(?,?,?,?,?,?)");
            $stmt->bind_param("iiddis",$tracker,$trackee,$lat,$lng,$status,$notification_id);
            $result = $stmt->execute();
            echo "status :" .$result;
            $stmt->close();
    }


    public function updateTracingStatus($tracker,$trackee,$status,$notification_id){
        $now = new DateTime();
        $date = $now->format('Y-m-d H:i:s');

        $stmt = $this->conn->prepare("UPDATE tracing_history  set status = ? , time_reached = ?  where tracker = ? AND trackee = ? AND notification_id = ?");
		$stmt->bind_param("isiis",$status,$date,$tracker,$trackee,$notification_id);
		$result = $stmt->execute();
		$stmt->close();
		if($result){
			return true;
		}
		return false;
    }


    public function getTraceData($mobile_number){

		$stmt = $this->conn->prepare(" SELECT  tracker,trackee,lat,lng,status,time_reached,notification_id FROM tracing_history where tracker = ?");
		$stmt->bind_param("i",$mobile_number);
        $stmt->execute();
        $res = $stmt->get_result();
        $trace_list = array();
        while($row = $res->fetch_array(MYSQLI_ASSOC)) {
            array_push($trace_list, $row);
        }
        $stmt->close();
        return $trace_list;
	}
}
?>
