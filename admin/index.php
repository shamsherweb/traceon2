<?php
require('layout/index_header.php');
require('include/functions.php');
echo getTotalDownloads();
?>

        <!-- Modal Start -->
        	<!-- Modal Task Progress -->	
	
		
	<!-- Modal Logout -->
	<div class="md-modal md-just-me" id="logout-modal">
		<div class="md-content">
			<h3><strong>Logout</strong> Confirmation</h3>
			<div>
				<p class="text-center">Are you sure want to logout from this awesome system?</p>
				<p class="text-center">
				<button class="btn btn-danger md-close">Nope!</button>
				<a href="login.html" class="btn btn-success md-close">Yeah, I'm sure</a>
				</p>
			</div>
		</div>
	</div>        <!-- Modal End -->	
	<!-- Begin page -->
	<div id="wrapper">
		
<!-- Top Bar Start -->
<div class="topbar">
    <div class="topbar-left">
        <div class="logo">
            <h1><a href="index.php"><img src="assets/img/logo.png" alt="Logo"></a></h1>
        </div>
        <button class="button-menu-mobile open-left">
        <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-collapse2">
                <ul class="nav navbar-nav hidden-xs">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-th"></i></a>
                        <div class="dropdown-menu grid-dropdown">
                            <div class="row stacked">
                               
                                <div class="col-xs-4">
                                    <a href="javascript:;" data-app="calc" data-status="inactive"><i class="fa fa-calculator"></i>Calculator</a>
                                </div>
                            </div>
                           
                            <div class="clearfix"></div>
                        </div>
                    </li>
                   
                </ul>
                <ul class="nav navbar-nav navbar-right top-navbar">
                   
                    <li class="dropdown iconify hide-phone"><a href="#" onClick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a></li>
                    <li class="dropdown topbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Danish <strong>Nadeem</strong> <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu">
                            
                            <li><a href="password.html">Change Password</a></li>
                            
                            <li class="divider"></li>
                            <li><a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->
		    <!-- Left Sidebar Start -->
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
               
                <div class="clearfix"></div>
                <!--- Profile -->
                <div class="profile-info">
                    
                    <div class="col-xs-12">
                        <div class="profile-text" style="padding-top:50px;">Welcome <b>Danish</b>
                           <span class="profile-buttons">
                         
                           <a href="javascript:;" title="Sign Out"><i class="fa fa-power-off text-red-1"></i></a>
                           </span>
                        </div>    
                    </div>
                </div>
                <!--- Divider -->
                <div class="clearfix"></div>
                <hr class="divider" />
                <div class="clearfix"></div>
                <!--- Divider -->
                <div id="sidebar-menu">
    <ul>
		<li>
			<a href="index.html"><i class=
			'icon-home-3'></i><span>Dashboard</span> <span class="pull-right"></span></a>
			
		</li>
		
		<li>
			<a href="users.html"><i class='fa fa-table'></i><span>All Users</span></a>

		</li>
		
		
	</ul>                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
        </div>
            <div class="left-footer">
                <div class="progress progress-xs">
                  <div class="progress-bar bg-green-1" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                    <span class="progress-precentage">80%</span>
                  </div>
                  
                  <a data-toggle="tooltip" title="See task progress" class="btn btn-default md-trigger" data-modal="task-progress"><i class="fa fa-inbox"></i></a>
                </div>
            </div>
        </div>
        <!-- Left Sidebar End -->		    	
		<!-- Start right content -->
        <div class="content-page">
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="content">
				<!-- Start info box -->
				<div class="row top-summary">
					<div class="col-lg-3 col-md-6">
						<div class="widget blue-2 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="icon-globe-inv"></i>
								</div>
								<div class="text-box">
									<p class="maindata">TOTAL <b>DOWNLOADS</b></p>
									<h2><span class="animate-number" data-value="<?php getTotalDownloads();
?>" data-duration="2000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget blue-3 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="icon-group"></i>
								</div>
								<div class="text-box">
									<p class="maindata">REGISTERED <b>USERS</b></p>
									<h2><span class="animate-number" data-value="6399" data-duration="3000">0</span></h2>

									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget green-1 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="fa fa-user"></i>
								</div>
								<div class="text-box">
									<p class="maindata">ACTIVE <b>USERS</b></p>
									<h2><span class="animate-number" data-value="70389" data-duration="3000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget yellow-1 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="fa fa-users"></i>
								</div>
								<div class="text-box">
									<p class="maindata">DISABLED <b>USERS</b></p>
									<h2><span class="animate-number" data-value="18648" data-duration="3000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

				</div>
				<!-- End of info box -->

				<div class="row">
					<div class="col-md-12">
						<div class="widget">
							<div class="widget-header transparent">
								<h2><strong>Recent</strong> Registered User</h2>
								<div class="additional-btn">
									<a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
									<a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
									<a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
								</div>
							</div>
							<div class="widget-content">
								<div class="data-table-toolbar">
									<div class="row">
										<div class="col-md-4">
											<form role="form">
											<input type="text" class="form-control" placeholder="Search...">
											</form>
										</div>
										<div class="col-md-8">
											<div class="toolbar-btn-action">
												
												<a class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
												<a class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
											</div>
										</div>
									</div>
								</div>
									
								<div class="table-responsive">
									<table data-sortable class="table table-hover table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th style="width: 30px" data-sortable="false"><input type="checkbox" class="rows-check"></th>
												<th>Full Name</th>
												<th>Location</th>
												<th>Date of birth</th>
												<th>Email</th>
												<th data-sortable="false">Sort : Off</th>
												<th>Status</th>
												<th data-sortable="false">Option</th>
											</tr>
										</thead>
										
										<tbody>
											<tr>
												<td>1</td><td><input type="checkbox" class="rows-check"></td><td><strong>John Doe</strong></td>
												<td>Yogyakarta, Indonesia</td><td>January 01, 1985</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>2</td><td><input type="checkbox" class="rows-check"></td><td><strong>Rusmanovski</strong></td>
												<td>Bali, Indonesia</td><td>september 21, 1995</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>3</td><td><input type="checkbox" class="rows-check"></td><td><strong>Annisa</strong></td>
												<td>London, UK</td><td>September 23, 1996</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>4</td><td><input type="checkbox" class="rows-check"></td><td><strong>Ari Rusmanto</strong></td>
												<td>Jakarta, Indonesia</td><td>January 01, 1990</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>5</td><td><input type="checkbox" class="rows-check"></td><td><strong>Jenny Doe</strong></td>
												<td>New York, US</td><td>March 11, 1975</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-danger">Suspended</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>6</td><td><input type="checkbox" class="rows-check"></td><td><strong>Hana Sartika</strong></td>
												<td>Semarang, Indonesia</td><td>June 23, 1991</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>7</td><td><input type="checkbox" class="rows-check"></td><td><strong>Tukimin</strong></td>
												<td>Surakarta, Indonesia</td><td>August 17, 1945</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>8</td><td><input type="checkbox" class="rows-check"></td><td><strong>Johnny Depp</strong></td>
												<td>Paris, French</td><td>October 30, 1972</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-warning">Deactivated</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>9</td><td><input type="checkbox" class="rows-check"></td><td><strong>Mas Arie</strong></td>
												<td>Boyolali, Indonesia</td><td>January 01, 1990</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
											<tr>
												<td>10</td><td><input type="checkbox" class="rows-check"></td><td><strong>Rusmanto</strong></td>
												<td>Bandung, Indonesia</td><td>February 28, 1992</td><td><a href="mailto:#">name@domain.com</a></td>
												<td>123</td><td><span class="label label-success">Active</span></td>
												<td>
													<div class="btn-group btn-group-xs">
														<a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
														
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
									
								<div class="data-table-toolbar">
									<ul class="pagination">
									  <li class="disabled"><a href="#">&laquo;</a></li>
									  <li class="active"><a href="#">1</a></li>
									  <li><a href="#">2</a></li>
									  <li><a href="#">3</a></li>
									  <li><a href="#">4</a></li>
									  <li><a href="#">5</a></li>
									  <li><a href="#">&raquo;</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
						
				</div>


				            <!-- Footer Start -->
            <footer>
                traceon &copy; 2016
                <div class="footer-links pull-right">
                	<a href="https://webnoo.com" target="_blank" title="Designed & Developed By WEBNOO" alt="Designed & Developed By Webnoo Technologies Pvt. Ltd."><img src="https://webnoo.com/client_digital_branding/images/logo-webnoo-branding.png" style="width: 28px;margin-top: -5px;margin-left: 10px;"></a>
                </div>
            </footer>
            <!-- Footer End -->			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->

        </div>
		<!-- End right content -->

	</div>
	<div id="contextMenu" class="dropdown clearfix">
		    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
		        <li><a tabindex="-1" href="javascript:;" data-priority="high"><i class="fa fa-circle-o text-red-1"></i> High Priority</a></li>
		        <li><a tabindex="-1" href="javascript:;" data-priority="medium"><i class="fa fa-circle-o text-orange-3"></i> Medium Priority</a></li>
		        <li><a tabindex="-1" href="javascript:;" data-priority="low"><i class="fa fa-circle-o text-yellow-1"></i> Low Priority</a></li>
		        <li><a tabindex="-1" href="javascript:;" data-priority="none"><i class="fa fa-circle-o text-lightblue-1"></i> None</a></li>
		    </ul>
		</div>
	<!-- End of page -->
		<!-- the overlay modal element -->
	<div class="md-overlay"></div>
	<!-- End of eoverlay modal -->
	<script>
		var resizefunc = [];
	</script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="assets/libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/libs/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="assets/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js"></script>
	<script src="assets/libs/jquery-detectmobile/detect.js"></script>
	<script src="assets/libs/jquery-animate-numbers/jquery.animateNumbers.js"></script>
	<script src="assets/libs/ios7-switch/ios7.switch.js"></script>
	<script src="assets/libs/fastclick/fastclick.js"></script>
	<script src="assets/libs/jquery-blockui/jquery.blockUI.js"></script>
	<script src="assets/libs/bootstrap-bootbox/bootbox.min.js"></script>
	<script src="assets/libs/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="assets/libs/jquery-sparkline/jquery-sparkline.js"></script>
	<script src="assets/libs/nifty-modal/js/classie.js"></script>
	<script src="assets/libs/nifty-modal/js/modalEffects.js"></script>
	<script src="assets/libs/sortable/sortable.min.js"></script>
	<script src="assets/libs/bootstrap-fileinput/bootstrap.file-input.js"></script>
	<script src="assets/libs/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="assets/libs/bootstrap-select2/select2.min.js"></script>
	<script src="assets/libs/magnific-popup/jquery.magnific-popup.min.js"></script> 
	<script src="assets/libs/pace/pace.min.js"></script>
	<script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="assets/libs/jquery-icheck/icheck.min.js"></script>

	<!-- Demo Specific JS Libraries -->
	<script src="assets/libs/prettify/prettify.js"></script>

	<script src="assets/js/init.js"></script>
	<!-- Page Specific JS Libraries -->
	<script src="assets/libs/d3/d3.v3.js"></script>
	<script src="assets/libs/rickshaw/rickshaw.min.js"></script>
	<script src="assets/libs/raphael/raphael-min.js"></script>
	<script src="assets/libs/morrischart/morris.min.js"></script>
	<script src="assets/libs/jquery-knob/jquery.knob.js"></script>
	<script src="assets/libs/jquery-jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="assets/libs/jquery-jvectormap/js/jquery-jvectormap-us-aea-en.js"></script>
	<script src="assets/libs/jquery-clock/clock.js"></script>
	<script src="assets/libs/jquery-easypiechart/jquery.easypiechart.min.js"></script>
	<script src="assets/libs/jquery-weather/jquery.simpleWeather-2.6.min.js"></script>
	<script src="assets/libs/bootstrap-xeditable/js/bootstrap-editable.min.js"></script>
	<script src="assets/libs/bootstrap-calendar/js/bic_calendar.min.js"></script>
	<script src="assets/js/apps/calculator.js"></script>
	<script src="assets/js/apps/todo.js"></script>
	<script src="assets/js/apps/notes.js"></script>
	<script src="assets/js/pages/index.js"></script>
    </body>
</html>