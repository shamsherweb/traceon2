<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Shamsher Ahmed
 * @version 1.0.1
 */

class DbHandler {
    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function getLogin($username,$password){

        $stmt = $this->conn->prepare("SELECT password FROM adminstrator WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->bind_result($saved_password);
        $stmt->store_result();
        $hash_password = md5($password,FALSE);
        
        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            if($hash_password == $saved_password){
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }

    public function getRegisteredUser(){
        $stmt = $this->conn->prepare("SELECT mobile_number FROM users");
        $stmt->execute();
        $stmt->store_result();
        $no_of_registered_user = $stmt->num_rows;
        return $no_of_registered_user;
    }

    public function getActiveUser(){
        $stmt = $this->conn->prepare("SELECT mobile_number FROM users where status = 1");
        $stmt->execute();
        $stmt->store_result();
        $no_of_active_user = $stmt->num_rows;
        return $no_of_active_user;
    }

    public \ getDisabledUser(){
        $stmt = $this->conn->prepare("SELECT mobile_number FROM users where status = 0");
        $stmt->execute();
        $stmt->store_result();
        $no_of_disabled_user = $stmt->num_rows;
        echo $no_of_disabled_user;
    }

    public function getAllRegisteredUser(){
        $stmt = $this->conn->prepare("SELECT name,country,email,status FROM users where status = 0");
        $stmt->execute();
        $stmt->store_result();
        $no_of_disabled_user = $stmt->num_rows;
        echo $no_of_disabled_user;
    }

    public function getAllRegisteredUserData(){
        $stmt = $this->conn->prepare(" SELECT name,country,email,mobile_number,status FROM users ");
		$result = $stmt->execute();
		if($result){
			$res = $stmt->get_result();
            $stmt->close();
			return $res;
		}else{
            $stmt->close();
			return NULL;
		}
    }
}
?>