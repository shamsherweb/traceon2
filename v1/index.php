  <?php
  require_once '../include/DbHandler.php';
  require_once '../include/PassHash.php';
  require '.././libs/Slim/Slim.php';

  \Slim\Slim::registerAutoloader();

  define( 'API_ACCESS_KEY', 'AIzaSyDz14iciyQfeo0lN2WXGmzh6N_fby7NiyI' );
  define('UPLOAD_DIR', '.././uploads/');
  $app = new \Slim\Slim();

  // User id from db - Global Variable
  $user_id = NULL;

  /**
   * Adding Middle Layer to authenticate every request
   * Checking if the request has valid api key in the 'Authorization' header
   */
  function authenticate(\Slim\Route $route) {
      // Getting request headers
      $headers = apache_request_headers();
      $response = array();
      $app = \Slim\Slim::getInstance();

      // Verifying Authorization Header
      if (isset($headers['Authorization'])) {
          $db = new DbHandler();

          // get the api key
          $api_key = $headers['Authorization'];
          // validating api key
          if (!$db->isValidApiKey($api_key)) {
              // api key is not present in users table
              $response["error"] = true;
              $response["message"] = "Access Denied. Invalid Api key";
              echoRespnse(401, $response);
              $app->stop();
          } else {
              //global $user_id;
              // get user primary key id;
              $user_id = $db->getMobileNumber($api_key);
          }
      } else {
          // api key is missing in header
          $response["error"] = true;
          $response["message"] = "Api key is misssing";
          echoRespnse(400, $response);
          $app->stop();
      }
  }

  // Methods with without Authentication
  /*
   * User Registration
   * url - /register
   * method - POST
   * params - mobile_number , name , email , password, country, gcm_id
   * //todo adding lat lng table
   * //todo checking whether mobile number exists or not in database
   */

  $app->post('/register', function() use ($app) {

              $response = array();

              $mobile_number = $app->request->post('mobile_number');
              $name = $app->request->post('name');
              $email = $app->request->post('email');
              $password = $app->request->post('password');
  			      $country = $app->request->post('country');
  			      $gcm_id = $app->request->post('gcm_id');

              validateEmail($email);

              $db = new DbHandler();
              $res = $db->createUser($mobile_number,$name, $email, $password,$country,$gcm_id);

              if ($res == USER_CREATED_SUCCESSFULLY) {
                  $response["error"] = false;
                  $response["message"] = "You are successfully registered";

                  $user = $db->getUserByMobileNumber($mobile_number);

                  $response["mobile_number"] = $mobile_number;
                  $response['name'] = $user['name'];
                  $response['email'] = $user['email'];
                  $response['apiKey'] = $user['api_key'];
                  $response['country'] =$user['country'];
                  $response['profile_url'] =$user['profile_url'];
                  $response['noOfFriends'] = $db->getNoOfFriends($mobile_number);;
                  $response['noOfPeopleTracking']= $db->getNumberOfFriendsTracking($mobile_number);

              } else if ($res == USER_CREATE_FAILED) {
                  $response["error"] = true;
                  $response["message"] = "Oops! An error occurred while registereing";
              } else if ($res == USER_ALREADY_EXISTED) {
                  $response["error"] = true;
                  $response["message"] = "user registered ,please login";
              }
              echoRespnse(201, $response);
          });

  /*
   * User Login
   * url - /login
   * method - POST
   * params - email, password
   */

  $app->post('/login', function() use ($app) {

              // reading post params
              $mobile_number = $app->request()->post('mobile_number');
              $password = $app->request()->post('password');
              $response = array();

              $db = new DbHandler();

              // check for correct email and password
              if ($db->checkLogin($mobile_number, $password)) {

                  // get the user by mobile_number
                  $user = $db->getUserByMobileNumber($mobile_number);

                  if ($user != NULL) {
  					          $response["mobile_number"] = $mobile_number;
                      $response["error"] = false;
                      $response['name'] = $user['name'];
                      $response['email'] = $user['email'];
                      $response['apiKey'] = $user['api_key'];
                      $response['country'] =$user['country'];
  					          $response['profile_url'] =$user['profile_url'];
  					          $response['message'] = "user successfully logged in";
  					          $response['noOfFriends'] = $db->getNoOfFriends($mobile_number);;
  					          $response['noOfPeopleTracking']= $db->getNumberOfFriendsTracking($mobile_number);
  					//$response['location'] =$location;
                } else {
                      // unknown error occurred
                      $response['error'] = true;
                      $response['message'] = "an error occurred ,please try again";
                  }
              } else {
                  // user credentials are wrong
                      $response['error'] = true;
                      $response['message'] = 'login failed,incorrect credentials';
              }

              echoRespnse(200, $response);
          });
  /*
   * User : Forgot Password
   * url - /login
   * method - POST
   * params - email_address
   */

  $app->post('/forgot_password', function() use ($app) {

        $email_address = $app->request->post('email_address');
  			$db = new DbHandler();
  			$response = array();
  			if($db->isUserExists($email_address)){
  			$result = $db->forgotPassword($email_address);
  			$user = $db->getUserByEmailAddress($email_address);
  			$to = $user['email'];
  			$name = $user['name'];

  			   if($result){
  				       emailForgotPassword($name,$to,$result);
  				       $response['error'] = false;
                 $response['message'] = "kindly check your mail you will receive pin shortly";
  			      }else{
  				       $response['error'] = true;
                 $response['message'] = "please enter correct email .";
  			      }
  			}else{
  				$response['error'] = true;
          $response['message'] = "user doesn't exists";
  			}
  			echoRespnse(200, $response);
      });

  /*
   * User : Check User exists or not in database
   * url - /login
   * method - POST
   * params - mobile_number
   *
   */


  $app->post('/check_user', function() use ($app) {

        $mobile_number = $app->request->post('mobile_number');
  			$db = new DbHandler();

  			$result = $db->isMobileNumberExists($mobile_number);

  			if($result){
  				$response['error'] = true;
          $response['message'] = "user is registered";
  				echoRespnse(202, $response);
  			}else{
  				$response['error'] = false;
          $response['message'] = "user is not registered";
  				echoRespnse(202, $response);
  			}
      });

  /*
   * ------------------------------------------------------ METHODS WITH AUTHENTICATION ------------------------------------------------------
   */

   /*
    * User Profile Information :
    * url - /login
    * method - POST
    * params - mobile_number
    */

   $app->post('/getprofileinformation', 'authenticate',function() use ($app) {

               // reading post params
               $mobile_number = $app->request()->post('mobile_number');
               $response = array();

               $db = new DbHandler();
               // check for correct email and password

                   // get the user by email
                   $user = $db->getUserByMobileNumber($mobile_number);

                   if ($user != NULL) {
                       $response["mobile_number"] = $mobile_number;
                       $response["error"] = false;
                       $response['name'] = $user['name'];
                       $response['email'] = $user['email'];
                       $response['apiKey'] = $user['api_key'];
                       $response['country'] =$user['country'];
                       $response['profile_url'] =$user['profile_url'];
                       $response['message'] = "user successfully logged in";
                       $response['noOfFriends'] = $db->getNoOfFriends($mobile_number);;
                       $response['noOfPeopleTracking']= $db->getNumberOfFriendsTracking($mobile_number);
             //$response['location'] =$location;
                   } else {
                       // unknown error occurred
                       $response['error'] = true;
                       $response['message'] = "An error occurred. Please try again";
                   }

               echoRespnse(200, $response);
           });


   $app->post('/change_password','authenticate',function() use ($app) {

        $mobile_number = $app->request->post('mobile_number');
  			$password = $app->request->post('password');
  			$db = new DbHandler();

  			$response = array();
  			$result = $db->changPassword($mobile_number,$password);

  			if($result){
  				$response['error'] = false;
          $response['message'] = "kindly check your mail you will receive pin shortly";
  				echoRespnse(202, $response);
  			}else{
  				$response['error'] = true;
          $response['message'] = "please enter correct mobile number";
  				echoRespnse(202, $response);
  			}

      });


  $app->post('/add_device', 'authenticate', function() use ($app) {

        $tracker = $app->request()->post('tracker');
  			$trackee = $app->request()->post('trackee');
        $email = $app->request()->post('email');
  			$name = $app->request()->post('name');

  			$response = array();

  			$db = new DbHandler();

  			if($tracker == $trackee){
  				$response["error"] = true;
          $response["message"] = "u cann't track yourself";
          echoRespnse(202, $response);
  			}else{

  				$isTrack = $db->isTracking($tracker, $trackee);

        	if($isTrack){
  					$response["error"] = true;
            $response["message"] = "you are already tracking this user";
            echoRespnse(202, $response);
  				}else{
  				$result = $db->isMobileNumberExists($trackee);
  					if($result != NULL){
  					       $status =$db->insertTrackingDetails($tracker,$trackee);
  					            if(!$status){
  							                 $user = $db->getUserInfo($trackee);
  							                 $sender = $db->getUserInfo($tracker);
  							                 $result = $db->getInviteList($trackee);
  							                 $no_of_invitations = $result->num_rows;
  							                 $to = $email;
  							                 $subject = "traceon new invitation";
  							                 $body = $no_of_invitations ." new pending request.";

  							emailInvite($user['name'],$user['email'],$sender['name']);
  							sendNotification($user['gcm_id'],$body,$subject);
  							$response["error"] = false;
  							$response["message"] = "tracking request sent successfully";
  							echoRespnse(202, $response);
  						}else{
  							$response["error"] = true;
  							$response["message"] = "kindly check the network connectivity";
  							echoRespnse(202, $response);
  						}
  					}else{
  						$response["error"] = true;
  						$response["message"] = "mobile number doesn't exists";
  						echoRespnse(202, $response);
  					}
  				}
  			}
      });

  $app->post('/getcontactlist', 'authenticate', function() use ($app) {

  			$mobile_number = $app->request->post('mobile_number');

        $response = array();
        $db = new DbHandler();
        $result = $db->getFriendList($mobile_number);
  			$response["error"] = true;
        $response["friend"] = array();
  			$flag = $result->num_rows;

  			if($flag>0){
  			while ($task = $result->fetch_assoc()) {
  				$response["error"] = false;
  				$friendnumber = $task["trackee"];
  				$friend = $db->getUserByMobileNumber($friendnumber);
          array_push($response["friend"], $friend);;
          }
  			}else{
          $response["error"] = true;
  			  $response["friend"] = array();
  			}
          echoRespnse(200, $response);
        });

  $app->post('/getlocation', 'authenticate', function() use ($app) {

  		$mobile_number = $app->request->post('mobile_number');
  		$page = $app->request->post('page');

  		$response = array();
      $history = array();
      $response["history"] = array();
      $response["error"] = false;

      $db = new DbHandler();
  		$result = $db-> getTimeLine($mobile_number);


  		$coordinate1 = $result->fetch_assoc();
  		$history["latitude"] = floatval($coordinate1["latitude"]);
  		$history["longitude"] = floatval($coordinate1["longitude"]);
  		$date = new DateTime($coordinate1["location_time"]);
  		$history["location_time"] = $date->format('Y-M-d H:i');
  		array_push($response["history"], $history);

  		while ($task = $result->fetch_assoc()) {
  				$distance = getDistance($history["latitude"],$history["longitude"],$task["latitude"],$task["longitude"]);
  				$history["distance"] = $distance;
  				$history["latitude"] = floatval($task["latitude"]);
  				$history["longitude"] = floatval($task["longitude"]);
  				$history["location_time"] = $task["location_time"];
  				$date = new DateTime($task["location_time"]);

  				$history["location_time"] = $date->format('Y-M-d H:i');
  				if($distance > 200){
  					array_push($response["history"], $history);
  				}
  			}
      $response["size"]=sizeof($response["history"]);

  		echoRespnse(200, $response);
  });

  $app->post('/get_location_time', 'authenticate', function() use ($app) {

  		$mobile_number = $app->request->post('mobile_number');
  		$page = $app->request->post('page');

  		$response = array();
          $db = new DbHandler();
  		$result = $db->getLocationHistory($mobile_number,$page);
  		$response["error"] = false;
          $response["history"] = array();
  		$history = array();

  		$coordinate1 = $result->fetch_assoc();

  		$history["latitude"] = floatval($coordinate1["latitude"]);
  		$history["longitude"] = floatval($coordinate1["longitude"]);
  		$history["location_time"] = $coordinate1["location_time"];


  		array_push($response["history"], $history);

  			while ($task = $result->fetch_assoc()) {

  			    $time_differrence = getTimeDifference($history["location_time"],$task['location_time']);

  				$history["distance"] =  $time_differrence;
  				$history["latitude"] = floatval($task["latitude"]);
  				$history["longitude"] = floatval($task["longitude"]);
  				$history["location_time"] = $task["location_time"];
          array_push($response["history"], $history);
  			}
  		echoRespnse(200, $response);
  });

  $app->post('/gettimeline', 'authenticate', function() use ($app) {

  		$mobile_number = $app->request->post('mobile_number');

  		$response = array();
          $db = new DbHandler();

  		$result = $db->getTimeLine($mobile_number);
  		$response["error"] = false;
          $response["history"] = array();
  		$history = array();
  			while ($task = $result->fetch_assoc()) {

  				$history["latitude"] = floatval($task["latitude"]);
  				$history["longitude"] = floatval($task["longitude"]);
  				$history["location_time"] = $task["location_time"];
                  array_push($response["history"], $history);;
              }
  		echoRespnse(200, $response);
  });

  $app->post('/gettimelinetemp', 'authenticate', function() use ($app) {

  		$mobile_number = $app->request->post('mobile_number');

  		$response = array();
          $db = new DbHandler();

  		$result = $db->getData($mobile_number);
  		$response["error"] = false;
          $response["history"] = array();
  		$history = array();
  			while ($task = $result->fetch_assoc()) {

  				$history["latitude"] = floatval($task["latitude"]);
  				$history["longitude"] = floatval($task["longitude"]);
  				$history["location_time"] = $task["location_time"];
                  array_push($response["history"], $history);;
              }
  		echoRespnse(200, $response);
  });

  $app->post('/suggestion', 'authenticate', function() use ($app) {

         		$mobile_number = $app->request->post('mobile_number');
            $name = $app->request->post('name');
  			    $suggestion = $app->request->post('suggestion');
            $response = array();
            $db = new DbHandler();

            $result = $db->reportApp($mobile_number, $name, $suggestion);
  			if($result){
  				$response["error"] = false;
        	$response["message"] = "your suggestion is necessary for us";
  			}else{
  				$response["error"] = true;
        	$response["message"] = "kindly check the network connectivity";
  			}
  	       echoRespnse(200, $response);
        });

  $app->post('/getinvitelist', 'authenticate', function() use ($app) {

         		$mobile_number = $app->request->post('mobile_number');
            $response = array();
            $db = new DbHandler();
            $result = $db->getInviteList($mobile_number);
  			    $response["error"] = false;
  			    $response["message"] ="just for testing";
            $response["friend"] = array();

  			$flag = $result->num_rows;
  			if($flag>0){
  			while ($task = $result->fetch_assoc()) {
  				$friendnumber = $task["tracker"];
  				$friend = $db->getUserByMobileNumber($friendnumber);
          array_push($response["friend"], $friend);;
              }
            }else{
  				$response["friend"] = null;
  			}
        echoRespnse(200, $response);
  });


  $app->post('/getpendinginvitelist', 'authenticate', function() use ($app) {

         		$mobile_number = $app->request->post('mobile_number');
              $response = array();
              $db = new DbHandler();
              $result = $db->getPendingInviteList($mobile_number);
  			$response["error"] = false;
  			$response["message"] ="testing";
              $response["friend"] = array();
  			$flag = $result->num_rows;
  			if($flag>0){

  			while ($task = $result->fetch_assoc()) {

  				$friendnumber = $task["trackee"];
  				$friend = $db->getUserByMobileNumber($friendnumber);
                  array_push($response["friend"], $friend);;
              }}else{
  				$response["friend"] = null;
  			}
             echoRespnse(200, $response);
          });


  $app->post('/delete', 'authenticate', function() use($app) {

  			$tracker_mobile_number = $app->request->post('tracker');
  			$trackee_mobile_number = $app->request->post('trackee');
              $db = new DbHandler();
              $response = array();
              $result = $db->removeContact($trackee_mobile_number,$tracker_mobile_number);

              if ($result) {
                  $response["error"] = false;
                  $response["message"] = "contact removed";
              } else {
                  $response["error"] = true;
                  $response["message"] = "failed to removed contact";
              }
              echoRespnse(200, $response);
          });


  $app->post('/accept', 'authenticate', function() use($app) {

  			$tracker_mobile_number = $app->request->post('tracker');
  			$trackee_mobile_number = $app->request->post('trackee');
              $db = new DbHandler();
              $response = array();
              $result = $db->acceptRequest($tracker_mobile_number,$trackee_mobile_number);

              if ($result) {
  				$sender = $db->getUserInfo($tracker_mobile_number);
  				$receiver = $db->getUserInfo($trackee_mobile_number);
  				$body = $receiver['name'] ." accepted your request.";
  				$subject = "accept notification";
  				emailAcceptInvitation($receiver['name'],$sender['email'],$sender['name']);
  				sendNotification($sender['gcm_id'],$body,$subject);

                  $response["error"] = false;
                  $response["message"] = "contact added";
              } else {
                  $response["error"] = true;
                  $response["message"] = "failed to add contact";
              }
              echoRespnse(200, $response);
          });

  $app->post('/upload', function() use($app) {

  	$baseAddress = "http://traceon.org/traceon/uploads/";
  	$imageId = $app->request->post('image');
  	$mobile_number = $app->request->post('mobile_number');
  	$db = new DbHandler();

  	#$img = str_replace('data:image/png;base64,', '', $imageId);
  	$img = str_replace(' ', '+', $imageId);
  	$data = base64_decode($img);
  	$file = UPLOAD_DIR . $mobile_number. '.jpg';
  	$profile_url = "http://traceon.org/traceon/uploads/" . $mobile_number .".jpg";
  	$success = file_put_contents($file, $data);

  	if($success){
  		$db->uploadProfilePic($mobile_number,$profile_url);
  		$response["error"] = false;
      $response['message'] = "image uploaded successfully";
  		echoRespnse(200, $response);
  	}
  });

  $app->post('/update_profile','authenticate', function() use($app) {

  	$mobile_number = $app->request->post('mobile_number');
  	$email = $app->request->post('email');
  	$name = $app->request->post('name');

  	$db = new DbHandler();

  	$result = $db->updateProfile($mobile_number,$name,$email);

  	if($result){
  		$user = $db->getUserByMobileNumber($mobile_number);
          $response["error"] = false;
          $response['name'] = $user['name'];
          $response['email'] = $user['email'];
  		$response['message'] = "user profile sucessfully updated";
  	}else{
  		 $response["error"] = true;
           $response["message"] = "try after some time";
  	}
  	 echoRespnse(200, $response);
  });

  $app->post('/get_profile_image','authenticate', function() use($app) {

  	$mobile_number = $app->request->post('mobile_number');

  });


  $app->post('/update_contact_list',function() use($app) {

  	$app->response->headers->set('Content-Type', 'application/json');

  	$data = json_decode($app->request->getBody());
  	$contacts = $data -> contacts;
  	$mod_contacts = array();
  	foreach( $contacts as $contact) {
  		$res = preg_replace("/[^0-9]/", "", $contact->mMobileNumber);
  		array_push($mod_contacts, $res);;
  	}
  	$serialize_contacts = serialize($mod_contacts);
  	$db = new DbHandler();
  	$mobile_number = $contacts = $data -> tracker;
  	$result = $db->updateContacts($serialize_contacts,$mobile_number);

  	if($result){
  		$response["error"] = true;
          $response["message"] = " contact list uploaded";
  	}else{
  		$response["error"] = true;
          $response["message"] = " please try after sometime";
  	}
  	echoRespnse(200, $response);
  });

  $app->post('/uploadlocation', 'authenticate', function() use ($app) {


  	$db = new DbHandler();
  	$response = array();
      $mobile_number = $app->request->post('mobile_number');
  	$lat = $app->request->post('lat');
  	$lng = $app->request->post('lng');
  	$date = $app->request->post('date');


  	$result = $db->insertLocationHistory($mobile_number,$lat,$lng,$date);


  		if($result){
  			$response["error"] = false;
  			$response["message"] = "location coordinate is uploaded successfully";
  		}else{
  			$response["error"] = true;
  			$response["message"] = "unable to save location online";
  	}

  		echoRespnse(200, $response);
  });

  $app->post('/set_geofencing',function() use($app) {

  	$app->response->headers->set('Content-Type', 'application/json');
  	$data = json_decode($app->request->getBody());
  	$db = new DbHandler();
  	$lat = $data->lat;
  	$lng = $data->lng;
  	$sender_mobile_number = $data->senderMobileNumber;
  	$mobile_numbers = $data ->contacts;

  	$_contacts = array();
  	$notification_ids = array();

  	foreach( $mobile_numbers as $contact) {
  		$res = $contact->mMobileNumber;
  		$notification_id = generateRandomString();
  		$notification_ids[$res] = $notification_id;
  		$db->insertTracingHistory($sender_mobile_number,$res,$lat,$lng,$notification_id);
  		array_push($_contacts, $res);
  	}

  	foreach( $mobile_numbers as $contact) {

  		$mob_no = $contact->mMobileNumber;
  		$notif_id = $notification_ids[$mob_no];
  		$gcm_id = $db->getGcmId($mob_no);
  		$title = "geofencing update";
  		$message = "traceon has set the geofencing location";
  		$gcmId= array($gcm_id);
  		sendLocationNotification($gcmId,$notif_id,$message,$title,$lat,$lng,$sender_mobile_number);
  	}

  		$response["error"] = false;
      $response["message"] = "geo location set successfully";

  	echoRespnse(200, $response);

  });

  $app->post('/tracing_history','authenticate',function() use($app) {

  	$db = new DbHandler();

  	$tracker = $app->request->post('tracker');

  	$response["error"] = false;
  	$response["message"] = "tracing history";
  	$response["tracing_history"] = array();

  	$trace_history = $db->getTraceData($tracker);

  	if($trace_history != null){

  		foreach($trace_history as $traceon_item){

  			$user = $db->getUserByMobileNumber($traceon_item['trackee']);
  			$user["lat"] = $traceon_item['lat'];
  			$user["lng"] = $traceon_item['lng'];
  			$user["status"] = $traceon_item['status'];
  			$user["reached_date"] = $traceon_item['time_reached'];
  			array_push($response["tracing_history"], $user);
  		}
  	} else{
  		$response["error"] = true;
  		$response["message"] = "error occurred while fetching tracing history";
  	}

  	echoRespnse(200, $response);

  });

  $app->post('/update_gcm',function() use($app){

  	$mobile_number = $app->request->post('mobile_number');
  	$gcm_id = $app->request->post('gcm_id');
  	$db = new DbHandler();
  	$result = $db->updateGcm($mobile_number,$gcm_id);
  	if($result){
  		$response["error"] = false;
  		$response['	'] = "user gcm id sucessfully updated";
  	}else{
  		$response["error"] = true;
           $response["message"] = "try after some time";
  	}
  	echoRespnse(200, $response);
  });


  $app->post('/send_notification', function() use ($app) {

  			$db = new DbHandler();
              $sender_mob_no = $app->request->post('sender_mobile_number');
  			$receiver_mob_no = $app->request->post('mobile_number');
  			$notification_id = $app->request->post('notification_id');

  			$senderGcmEmail = $db->getGcmEmail($sender_mob_no);
  			$senderInformation = $db->getUserByMobileNumber($sender_mob_no);
  			$receiverInformation = $db->getUserByMobileNumber($receiver_mob_no);

  			$status = 1;
  			$db->updateTracingStatus($sender_mob_no,$receiver_mob_no,$status,$notification_id);


  			$senderEmail =  $senderGcmEmail['email'];
  			$senderGcm = $senderGcmEmail['gcm_id'];
  			$senderName = $senderInformation['name'];
  			$receiverName = $receiverInformation['name'];

  			$subject = "tracon tracing";
  			$body = $receiverName." reached successfully.";

  			emailGeolocationUpdate($senderName,$senderEmail,$receiverName);
  			sendNotification($senderGcm,$body,$subject);


  			$response['error'] = false;
              $response['message'] = "notification successfully send";

  			echoRespnse(200, $response);
  });




  $app->post('/get_all_device_list',function() use($app){

  	$mobile_number = $app->request->post('mobile_number');
  	$response["error"] = true;
      $response["friend"] = array();
  	$device_list_array = array();
  	$db = new DbHandler();

  	$device_list = $db->getAllDeviceList();
  	$contact_list_mobile_numbers = $db->getContactListMobileNumbers($mobile_number);
  	$user_contact_list_serialized = $db->getContacts($mobile_number);
  	$user_contact_list_unselized = unserialize($user_contact_list_serialized);

  	$contact_list = array();

  	 while ($task = $contact_list_mobile_numbers->fetch_assoc()) {
  				array_push($contact_list, $task['trackee']);
              }

  	 while ($task = $device_list->fetch_assoc()) {
  				array_push($device_list_array, $task['mobile_number']);
       }

  	 if (($key = array_search($mobile_number, $device_list_array)) !== false) {
      		unset($device_list_array[$key]);
        }

  		foreach( $device_list_array as $device) {
  				$response["error"] = false;
  				$friend = $db->getUserByMobileNumber($device);

  				if ((($key = array_search($device, $contact_list)) !== false)){

  						$friend["track"] = "1";
  						array_push($response["friend"], $friend);;

  				}else{
  					if(array_search($device, $user_contact_list_unselized)){
  						$friend["track"] = "0";
  						array_push($response["friend"], $friend);;
  					}
  				}
  		}
          echoRespnse(200, $response);

  });

  function getTimeDifference($start_date,$end_time){

  	$start = new DateTime($start_date);
  	$end = new DateTime($end_time);


  	$difference_time = $start->diff($end);

  	$elapsed =$difference_time->format( '%i minutes');

  	return $elapsed;
  }


  function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

  /**
   * Verifying required params posted or not
   */
  function verifyRequiredParams($required_fields) {
      $error = false;
      $error_fields = "";
      $request_params = array();
      $request_params = $_REQUEST;
      // Handling PUT request params
      if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
          $app = \Slim\Slim::getInstance();
          parse_str($app->request()->getBody(), $request_params);
      }
      foreach ($required_fields as $field) {
          if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
              $error = true;
              $error_fields .= $field . ', ';
          }
      }

      if ($error) {
          // Required field(s) are missing or empty
          // echo error json and stop the app
          $response = array();
          $app = \Slim\Slim::getInstance();
          $response["error"] = true;
          $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
          echoRespnse(400, $response);
          $app->stop();
      }
  }

  /**
   * Validating email address
   */
  function validateEmail($email) {
      $app = \Slim\Slim::getInstance();
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          $response["error"] = true;
          $response["message"] = 'Email address is not valid';
          echoRespnse(400, $response);
          $app->stop();
      }
  }

  /**
   * Echoing json response to client
   * @param String $status_code Http response code
   * @param Int $response Json response
   */
  function echoRespnse($status_code, $response) {
      $app = \Slim\Slim::getInstance();
      // Http response code
      $app->status($status_code);

      // setting response content type to json
      $app->contentType('application/json');

      echo json_encode($response);
  }

  function sendNotification($gcmId,$message,$title){
  	$registrationIds = array($gcmId);

  // prep the bundle
  $msg = array
  (
  	'message' 	=> $message,
  	'title'		=> $title,
  	'subtitle'	=> 'This is a subtitle. subtitle',
  	'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
  	'vibrate'	=> 1,
  	'geo_location'		=> 0,
  	'largeIcon'	=> 'large_icon',
  	'smallIcon'	=> 'small_icon'
  );
  $fields = array
  (
  	'registration_ids' 	=> $registrationIds,
  	'data'			=> $msg
  );

  $headers = array
  (
  	'Authorization: key=' . API_ACCESS_KEY,
  	'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );
  }


  function sendLocationNotification($gcmId,$notif_id,$message,$title,$lat,$lng,$sender_mobile_number){
  	$registrationIds = $gcmId;

  // prep the bundle
  $msg = array
  (
  	'message' 	=> $message,
  	'title'		=> $title,
  	'notification_id' =>$notif_id,
  	'lat'	=> $lat,
  	'lng'	=> $lng,
  	'sender_mobile_number'	=> $sender_mobile_number,
  	'geo_location'	=> 1,
  	'largeIcon'	=> 'large_icon',
  	'smallIcon'	=> 'small_icon'
  );
  $fields = array
  (
  	'registration_ids' 	=> $registrationIds,
  	'data'			=> $msg
  );

  $headers = array
  (
  	'Authorization: key=' . API_ACCESS_KEY,
  	'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );
  }

  function sendEmail($name,$to,$subject,$body){

  	require_once "Mail.php";

  	$from = "info-noreply@traceon.org";
  	$host = "us2.smtp.mailhostbox.com";
  	$port = "25";
  	$username = "info-noreply@traceon.org";
  	$password = "@Traceon123";

  	$headers = array ('From' => $from,
  	 'To' => $to,
  	'Subject' => $subject);
  	$smtp = Mail::factory('smtp',
  	 array ('host' => $host,
  		'port' => $port,
  		'auth' => true,
  		'username' => $username,
  		'password' => $password));

  	$mail = $smtp->send($to, $headers, $body);
  }

  function emailForgotPassword($name,$to,$passcode){

  	require_once "Mail.php";
  	require_once "Mail/mime.php";


  	//These configuration will be saved later on in Config.php (Reminder)
  	$from = "info-noreply@traceon.org";
  	$host = "us2.smtp.mailhostbox.com";
  	$port = "25";
  	$username = "info-noreply@traceon.org";
  	$password = "@Traceon123";

  	$subject = "Password Reset \r\n\r\n";
  	$text = "Password Reset";

  	$html = "<html><body><p> Dear ".$name.",<br><br>Your PIN of traceon has been changed to ".$passcode." because someone at the IP address 103.43.35.169 requested a password reset for your account <br>"
  	."<br>Thank you for using traceon ! <br> <br>Warm Regards, <br> traceon.org </p> </body></html>";
  	$crlf = "\n";
  	$mime = new Mail_mime($crlf);
  	$mime->setTXTBody($text);
  	$mime->setHTMLBody($html);

  	$headers = array ('From' => $from,
      'To' => $to,
      'Subject' => $subject);
  		$smtp = Mail::factory('smtp',
   			 array ('host' => $host,
     			 'auth' => true,
      		 'username' => $username,
      		 'password' => $password));


  	$body = $mime->get();
  	$headers = $mime->headers($headers);

  	$mail = $smtp->send($to, $headers, $body);

  }

  function emailGeolocationUpdate($receiver,$to,$sender){

  	require_once "Mail.php";
  	require_once "Mail/mime.php";


  	//These configuration will be saved later on in Config.php (Reminder)
  	$from = "info-noreply@traceon.org";
  	$host = "us2.smtp.mailhostbox.com";
  	$port = "25";
  	$username = "info-noreply@traceon.org";
  	$password = "@Traceon123";

  	$subject = "user successfully reached the destination | traceon.org";
  	$text = "Invitation";

  	$html = "<html><body><p> Dear ".$receiver.",<br><br>". $sender." has reached destination."
  	."<br>   <br>Warm Regards, <br> traceon.org </p> </body></html>";
  	$crlf = "\n";
  	$mime = new Mail_mime($crlf);
  	$mime->setTXTBody($text);
  	$mime->setHTMLBody($html);

  	$headers = array ('From' => $from,
    'To' => $to,
    'Subject' => $subject);
  		$smtp = Mail::factory('smtp',
   			 array ('host' => $host,
     			 'auth' => true,
      		 'username' => $username,
      		 'password' => $password));


  	$body = $mime->get();
  	$headers = $mime->headers($headers);

  	$mail = $smtp->send($to, $headers, $body);

  }

  function emailInvite($receiver,$to,$sender){

  	require_once "Mail.php";
  	require_once "Mail/mime.php";


  	//These configuration will be saved later on in Config.php (Reminder)
  	$from = "info-noreply@traceon.org";
  	$host = "us2.smtp.mailhostbox.com";
  	$port = "25";
  	$username = "info-noreply@traceon.org";
  	$password = "@Traceon123";

  	$subject = "Invitation to trace you on traceon | traceon.org";
  	$text = "Invitation";

  	$html = "<html><body><p> Dear ".$receiver.",<br><br>". $sender." wants to trace you using traceon app and invites you to join on traceon.". "<br> <br> traceOn is an application designed by OSRIK Technologies, which assists in tracking any smartphone you want to monitor". "Ensure the security of all your family members, friends or employees and more, by just clicking a button."
  	."<br> <a href=\"https://play.google.com/store/apps/details?id=com.webnoo.traceon\"> Accept Invitation  ! traceon</a> <br> <br>Warm Regards, <br> traceon.org </p> </body></html>";
  	$crlf = "\n";
  	$mime = new Mail_mime($crlf);
  	$mime->setTXTBody($text);
  	$mime->setHTMLBody($html);

  	$headers = array ('From' => $from,
    'To' => $to,
    'Subject' => $subject);
  		$smtp = Mail::factory('smtp',
   			 array ('host' => $host,
     			 'auth' => true,
      		 'username' => $username,
      		 'password' => $password));


  	$body = $mime->get();
  	$headers = $mime->headers($headers);

  	$mail = $smtp->send($to, $headers, $body);

  }

  function emailAcceptInvitation($receiver,$to,$sender){

  	require_once "Mail.php";
  	require_once "Mail/mime.php";


  	//These configuration will be saved later on in Config.php (Reminder)
  	$from = "info-noreply@traceon.org";
  	$host = "us2.smtp.mailhostbox.com";
  	$port = "25";
  	$username = "info-noreply@traceon.org";
  	$password = "@Traceon123";

  	$subject = $receiver ." is now on traceon | traceon.org";
  	$text = "Accept Invitation";

  	$html = "<html><body><p> Dear ".$sender.",<br><br>". $receiver." is now on traceon! <br>"
  	.$receiver." has accepted your invitation and joined traceon. You can now easily track "
  	.$receiver." by visiting the contact section on traceon app <br>"
  	."<br> <br> <br>Warm Regards, <br> traceon.org </p> </body></html>";
  	$crlf = "\n";
  	$mime = new Mail_mime($crlf);
  	$mime->setTXTBody($text);
  	$mime->setHTMLBody($html);

  	$headers = array ('From' => $from,
    'To' => $to,
    'Subject' => $subject);
  		$smtp = Mail::factory('smtp',
   			 array ('host' => $host,
     			 'auth' => true,
      		 'username' => $username,
      		 'password' => $password));


  	$body = $mime->get();
  	$headers = $mime->headers($headers);

  	$mail = $smtp->send($to, $headers, $body);

  }

  function getDistance($from_lat,$from_lon,$to_lat,$to_lon){
  	$theta = $from_lon - $to_lon;
  	$dist = sin(deg2rad($from_lat)) * sin(deg2rad($to_lat)) +  cos(deg2rad($from_lat)) * cos(deg2rad($to_lat)) * cos(deg2rad($theta));
  	$dist = acos($dist);
      $dist = rad2deg($dist);
  	$miles = $dist * 60 * 1.1515;
  	$distace = ($miles * 1.609344) * 1000;

  	return $distace;
  }
  $app->run();
  ?>
